package guru.springframework

class Sum(
    val augmend: Expression,
    val addmend: Expression
    ) : Expression {
    override fun reduce(bank: Bank, to: String): Money {
        val amount = augmend.reduce(bank, to).amount + addmend.reduce(bank, to).amount
        return Money(amount, to)
    }

    override fun plus(added: Expression): Expression  = Sum(this, added)
}