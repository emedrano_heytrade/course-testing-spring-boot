package guru.springframework

class Bank {
    private val rateMap: HashMap<Pair, Int> = HashMap()
    fun reduce(source: Expression, toCurrency: String): Money = source.reduce(this, toCurrency)
    fun rate(from: String, to: String): Int {
        if (from == to) return 1
        return rateMap[Pair(from, to)]!!
    }
    fun addRate(from: String, to: String, rate: Int) {
        rateMap[Pair(from,to)] = rate
    }
}
