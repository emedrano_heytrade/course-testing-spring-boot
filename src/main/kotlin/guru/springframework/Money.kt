package guru.springframework

data class Money(
    val amount: Int,
    private val currency: String
    ): Expression {
    companion object {
        fun dollar(amount: Int) = Money(amount, "USD")
        fun franc(amount: Int) = Money(amount, "CHF")
    }

    fun times (multiplier:Int): Expression = Money(amount * multiplier, this.currency)

    internal fun currency(): String = this.currency

    override fun reduce(bank: Bank, to: String): Money {
        return Money(amount/ bank.rate(this.currency, to), to)
    }

    override fun plus(added: Expression): Expression  = Sum(this, added)

    override fun equals(other: Any?): Boolean {
        val money = other as Money
        return this.amount == money.amount && this.currency == money.currency
    }

    override fun toString(): String {
        return "Money{" +
                "amount = $amount," +
                "currency $currency" +
                "}"
    }

    override fun hashCode(): Int {
        var result = amount
        result = 31 * result + currency.hashCode()
        return result
    }


}

