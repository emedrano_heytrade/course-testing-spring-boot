package guru.springframework

interface Expression {
    fun reduce(bank: Bank, to: String): Money
    fun plus(added: Expression) : Expression
}