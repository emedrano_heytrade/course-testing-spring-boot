package guru.springframework

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class PairTest {

    @Test
    fun `Pair recognizes when two objects are the same`(){
        val pair = Pair(from = "", to = "")
        assertEquals(pair, pair)
    }

    @Test
    fun `Pair recognizes when two objects are not the same`(){
        val pair = Pair(from = "", to = "")
        val sum = Sum(
            augmend = Money(amount = 0, currency = ""),
            addmend = Money(amount = 0, currency = "")
        )

        assertNotEquals(pair, sum)
    }


}