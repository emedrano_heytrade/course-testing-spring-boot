package guru.springframework

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test

class MoneyTest {

    @Test
    internal fun multiplication() {
        val fiveFranc = Money.franc(5)
        assertEquals(Money.franc(10),fiveFranc.times(2))
        assertEquals(Money.franc(15),fiveFranc.times(3))
        val fiveDollar = Money.dollar(5)
        assertEquals(Money.dollar(10),fiveDollar.times(2))
        assertEquals(Money.dollar(15),fiveDollar.times(3))
    }

    @Test
    internal fun equalityFranc() {
        assertEquals(Money.franc(5), Money.franc(5))
        assertNotEquals(Money.franc(5), Money.franc(8))
        assertEquals(Money.dollar(5), Money.dollar(5))
        assertNotEquals(Money.dollar(4), Money.franc(4))
    }

    @Test
    internal fun currencies() {
        assertEquals("USD", Money.dollar(1).currency())
        assertEquals("CHF", Money.franc(1).currency())
    }

    @Test
    internal fun tostring() {
        assertEquals("Money{amount = 1,currency USD}",Money.dollar(1).toString())
        assertEquals("Money{amount = 1,currency CHF}",Money.franc(1).toString())
    }

    @Test
    internal fun simpleAddition() {
        val five = Money.dollar(5)
        val sum : Expression = five.plus(five)
        val bank = Bank()
        val reduced = bank.reduce(sum, "USD")
        assertEquals(Money.dollar(10), reduced)
    }

    @Test
    internal fun plusReturnSum() {
        val five = Money.dollar(5)
        val result = five.plus(five)
        val sum = result as Sum
        assertEquals(five, sum.augmend)
        assertEquals(five, sum.addmend)
    }

    @Test
    internal fun reduceSum() {
        val sum: Expression = Sum(Money.dollar(3), Money.dollar(4))
        val bank = Bank()
        val result: Money = bank.reduce(sum, "USD")

        assertEquals(Money.dollar(7), result)
    }

    @Test
    internal fun reduceMoney() {
        val bank = Bank()

        val result = bank.reduce(Money.dollar(1), "USD")

        assertEquals(Money.dollar(1), result)
    }

    @Test
    fun testReduceMoneyDifferentCurrency(){
        val bank = Bank()
        bank.addRate("CHF", "USD", 2)

        val result = bank.reduce(Money.franc(2), "USD")

        assertEquals(Money.dollar(1), result)
    }

    @Test
    fun testIdentityRate(){
        assertEquals(1, Bank().rate("USD", "USD"))
        assertEquals(1, Bank().rate("CHF", "CHF"))
    }

    @Test
    fun testMixedAddition(){
        val fiveBucks = Money.dollar(5)
        val tenFrancs = Money.franc(10)
        val bank = Bank()
        bank.addRate("CHF", "USD", 2)

        val result = bank.reduce(fiveBucks.plus(tenFrancs), "USD")

        assertEquals(Money.dollar(10), result)
    }


}